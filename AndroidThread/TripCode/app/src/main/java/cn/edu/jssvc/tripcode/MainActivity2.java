package cn.edu.jssvc.tripcode;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

public class MainActivity2 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        Intent intent = getIntent();
        String code = intent.getStringExtra("code");

        TextView textView = findViewById(R.id.textView2);

        ImageView imageView = findViewById(R.id.imageView);

        textView.setText(code);

        if (code.equals("1")){
            imageView.setImageResource(R.drawable.ic_baseline_arrow_upward_green);
        }else {
            imageView.setImageResource(R.drawable.ic_baseline_arrow_upward_yellow);
        }
    }
}