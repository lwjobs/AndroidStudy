package cn.edu.jssvc.tripcode;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView textView;
    private ProgressBar progressBar;
    private EditText editText;

    private Button button;

    private int progress = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textView = findViewById(R.id.textView);
        editText = findViewById(R.id.editTextTextPersonName);
        progressBar = findViewById(R.id.progressBar);

        button = findViewById(R.id.button);


        final Runnable runnable = new Runnable() {
            @Override
            public void run() {
                while (progress<100){

                    progress++;

                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            textView.setText(""+progress);
                        }
                    });

                    try {
                        Thread.sleep(10);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                progress = 0;
                Intent intent = new Intent(MainActivity.this,MainActivity2.class);
                intent.putExtra("code",editText.getText().toString().trim());
                startActivity(intent);
            }
        };

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Thread thread = new Thread(runnable);
                thread.start();
                progressBar.setVisibility(View.VISIBLE);
            }
        });

    }
}