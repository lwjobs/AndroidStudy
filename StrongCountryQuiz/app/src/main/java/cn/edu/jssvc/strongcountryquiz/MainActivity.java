package cn.edu.jssvc.strongcountryquiz;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private boolean isCheckedOptionA = false;
    private boolean isCheckedOptionB = false;
    private boolean isCheckedOptionC = false;
    private boolean isCheckedOptionD = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        CheckBox checkBoxOptionA = findViewById(R.id.checkBoxOptionA);
        CheckBox checkBoxOptionB = findViewById(R.id.checkBoxOptionB);
        CheckBox checkBoxOptionC = findViewById(R.id.checkBoxOptionC);
        CheckBox checkBoxOptionD = findViewById(R.id.checkBoxOptionD);

        Button buttonNext = findViewById(R.id.buttonNext);

        checkBoxOptionA.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                isCheckedOptionA = isChecked;
            }
        });

        checkBoxOptionB.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                isCheckedOptionB = isChecked;
            }
        });

        checkBoxOptionC.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                isCheckedOptionC = isChecked;
            }
        });

        checkBoxOptionD.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                isCheckedOptionD = isChecked;
            }
        });

        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isCheckedOptionA && isCheckedOptionB && isCheckedOptionC && isCheckedOptionD){
                    Toast.makeText(MainActivity.this,"回答正确",Toast.LENGTH_SHORT).show();
                }else {
                    Toast.makeText(MainActivity.this,"回答错误",Toast.LENGTH_SHORT).show();
                }
            }
        });
    }
}