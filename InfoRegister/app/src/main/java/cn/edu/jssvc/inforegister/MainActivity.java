package cn.edu.jssvc.inforegister;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private EditText editTextName, editTextPhone;

    private int checkedIdSex;
    private boolean isCheckJava = false;
    private boolean isCheckAndroid = false;
    private boolean isCheckMath = false;
    private boolean isCheckEnglish = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editTextName = findViewById(R.id.editTextTextPersonName);
        editTextPhone = findViewById(R.id.editTextTextPersonPhone);

        RadioGroup radioGroupSex = findViewById(R.id.radioGroupsex);

        CheckBox checkBoxAndroid = findViewById(R.id.checkBoxAndroid);
        CheckBox checkBoxJava = findViewById(R.id.checkBoxJava);
        CheckBox checkBoxMath = findViewById(R.id.checkBoxMath);
        CheckBox checkBoxEnglish = findViewById(R.id.checkBoxEnglish);

        Button buttonCheck = findViewById(R.id.buttonCheck);

        radioGroupSex.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int id) {
                checkedIdSex = id;
            }
        });

        checkBoxJava.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                isCheckJava = isChecked;
            }
        });

        checkBoxMath.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                isCheckMath = isChecked;
            }
        });

        checkBoxAndroid.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                isCheckAndroid = isChecked;
            }
        });

        checkBoxEnglish.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                isCheckEnglish = isChecked;
            }
        });

        buttonCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = editTextName.getText().toString().trim();
                String phone = editTextPhone.getText().toString().trim();
                String sex = checkedIdSex == R.id.radioButtonMale ? "男" : "女";
                String android = isCheckAndroid ? "Android" : "";
                String java = isCheckJava ? "Java" : "";
                String math = isCheckMath ? "数学" : "";
                String english = isCheckEnglish ? "英语" : "";

                Toast.makeText(MainActivity.this, "姓名：" + name + "，手机：" + phone +
                                "，性别："+sex +"，兴趣："+ android + java + math + english,
                        Toast.LENGTH_SHORT).show();
            }
        });
    }
}